use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

const BUFFER_CAPACITY: usize = 16 * 1024;

fn main() {
    let file = File::open("test.txt");

    let mut file = match file {
        Ok(file) => file,
        Err(error) => {
            panic!("Could not file properly {:?}", error)
        },
    };

    let mut reader = BufReader::with_capacity(BUFFER_CAPACITY, file);

    let mut buffer: [u8; BUFFER_CAPACITY] = [0; BUFFER_CAPACITY];

    let mut total_bytes: usize = 0;

    loop {
        match reader.read(&mut buffer) {
            Err(error) => {
                panic!("Could not read file {:?}", error)
            },
            Ok(bytes_read) => {
                if bytes_read == 0 {
                    break;
                }
                total_bytes += bytes_read;
            },
        };
    }
    println!("{}", total_bytes);
}
